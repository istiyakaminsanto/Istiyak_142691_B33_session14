<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <script src="assets/bootstrap.min.js"></script>
    <script src="assets/jquery.min.js"></script>
    <style>
        .box{
            height: 320px;
            width: 600px;
            background: #00FF40;
            padding: 18px;
            margin: 0 auto;
            border-style: solid;
            border-color: #0000ff;

        }
    </style>

</head>
<body>
<form>
    <div class="box">

        <div class="form-group">
            <label for="exampleInputEmail1">Email :</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email">

        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Author Name :</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Author Name">
        </div>



        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input">
                Check me out
            </label>

        </div>
        <div>
            <button type="submit" class="btn btn-info">Save</button>
            <button type="submit" class="btn btn-primary">Edit</button>
            <button type="submit" class="btn btn-danger">Delete</button>
        </div>




    </div>

</body>
</html>